[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/publicom/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/publicom)

# ![Logo](site/static/images/logos/fedigov_logo.svg) Welcome to Fedigov

## License

Author: Ralf Hersel and FSFE contributors

License: GPL3

Repository: [publicom/site an main - publicom - FSFE Git Service](https://git.fsfe.org/fsfe-system-hackers/publicom/src/branch/main/site)

## Contact

https://matrix.to/#/@ralfhersel:fsfe.org
