## Release Notes

###### Version 0.03 - 2022.05.18

* Intro subtitel changed
* Masto-logos removed
* Three Fedi-Services section changes

###### Version 0.02 - 2022.05.16

* Max's i18n changes

###### Version 0.01 - 2022.05.10

* Initial release

